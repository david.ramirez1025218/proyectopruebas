/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import controlador.SucursalControl;
import controlador.ed.cola.Cola;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Pila;
import controlador.exception.Pilai.TopExcetion;
import controlador.util.Utilidades;
import modelo.Sucursal;

/**
 *
 * @author crsitian
 */
public class Main {

    public static void main(String[] args) throws TopExcetion, VacioException, PosicionException {

        /**
         *
         * SucursalControl sc = new SucursalControl();
         *
         * try {
         *
         * sc.getSucursal().setId(1); sc.getSucursal().setNombre("Pool");
         * sc.registrar(); sc.setSucursal(null);
         *
         *
         * sc.getSucursal().setId(2); sc.getSucursal().setNombre("Cristian");
         * sc.registrar(); sc.setSucursal(null);
         *
         * sc.getSucursal().setId(3); sc.getSucursal().setNombre("Guanchope");
         * sc.registrar(); sc.setSucursal(null);
         *
         * sc.getSucursal().setId(4); sc.getSucursal().setNombre("David");
         * sc.registrar(); Utilidades.imprimir(sc.getSucursal().getVentas());
         * //lista de ventas //System.out.println(sc.getSucursal().getVentas());
         * sc.setSucursal(null);
         *
         * //sc.getSucursal().setId(5); //sc.getSucursal().setNombre("NONE");
         * //sc.registrar(); //sc.setSucursal(null);
         *
         *
         *
         *
         * Utilidades.imprimir(sc.getSucursales()); //lista de sucursales
         *
         * } catch (Exception e) { System.out.println("Error " +
         * e.getMessage()); Utilidades.imprimir(sc.getSucursales());
        }
         */
        
        Pila<Integer> pila = new Pila(5);
        Cola<Integer> cola = new Cola(5);
        ListaEnlazada<String> lista = new ListaEnlazada();
        
        try {
            pila.push(5);
            pila.push(56);
            pila.push(9);
            pila.push(45);
            //pila.push(8);
            pila.Print();
            pila.pop();
            
            System.out.println("-----COLAA-----");
            cola.queque(5);
            cola.queque(56);
            cola.queque(9);
            cola.queque(45);
            //cola.queque(8);
            cola.print();
            cola.dequeque();
            
            } catch (VacioException ex) {
                System.out.println("--------------");
            }
    }
}
    
            /*
            lista.insertar("cris");
            lista.insertar("pedro");
            lista.insertarInicio("Maria");
            lista.insertar("Juan");
            lista.insertarPosicion("Alice", 3);
            lista.Imprimir();
            //System.out.println(lista.get(3));
            System.out.println("---------------");
            System.out.println("---Eliminar---");
            System.out.println(lista.delete(3));
            lista.Imprimir();
        } catch (VacioException ex) {
            System.out.println(ex.getMessage());
        } catch (PosicionException ex) {
            System.out.println(ex.getMessage());
        }
        
        
        
        /**
        ListaEnlazada<Sucursal> lista = new ListaEnlazada();
        try {
            Sucursal s1 = new Sucursal();
            
            s1.setId(lista.size()+1);
            s1.setNombre("Alice");
            
            lista.insertar(s1);
            lista.Imprimir();
            
            s1 = new Sucursal();
            
            s1.setId(lista.size()+1);
            s1.setNombre("Cristian");
            
            lista.insertar(s1);
            lista.Imprimir();
            
        } catch (Exception e) {
        }
        */

