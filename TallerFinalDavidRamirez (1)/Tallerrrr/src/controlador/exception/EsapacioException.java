/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.exception;

/**
 *
 * @author crsitian
 */
public class EsapacioException extends Exception {

    public EsapacioException(String message) {
        super(message);
    }
    
    public EsapacioException() {
        super("No hay espacio suficiente");
    }
    
}
