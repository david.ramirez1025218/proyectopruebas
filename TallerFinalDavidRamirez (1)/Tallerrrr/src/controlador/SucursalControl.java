/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.exception.EsapacioException;
import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author crsitian
 */
public class SucursalControl {

    private ListaEnlazada<Sucursal> sucursales;
    private ListaEnlazada<Venta> venta;
    private Sucursal sucursal;
    private Venta ventas;
    
    private int id = 0;

    private ListaEnlazada<Venta> listaVentas = new ListaEnlazada<>();
    
    /*public boolean registrar() throws EsapacioException {
        int pos = -1;
        int cont = -1;

        for (Sucursal s : getSucursales()) {
            cont++;
            if (s == null) {
                pos = cont;
                break;
            }

        }

        if (pos == -1) {
            throw new EsapacioException();
        }

        
        sucursal.setVentas(new Venta[EnumMes.values().length]);
        for (int i = 0; i < EnumMes.values().length; i++) {
            Venta venta = new Venta();
            venta.setId(i + 1);
            venta.setMes(EnumMes.values()[i]);
            venta.setValor(0.0);
            sucursal.getVentas()[i] = venta;
        }
        
        sucursales[pos] = sucursal;
        return true;
    } */
    
    public boolean guardarVentas(Integer posVenta, Double valor) throws EsapacioException, VacioException, PosicionException {
    if (this.sucursal != null) {
            if (posVenta <= this.sucursal.getVentas().size() - 1) {
                sucursal.getVentas().get(posVenta).setValor(valor);
            } else {
                throw new EsapacioException();
            }
        } else {
            throw new NullPointerException("Debe seleccionar una sucursal: ");
        }

        return true;
    }
    

    /**
     * @return the venta
     */
    public Venta getVenta() {
        return ventas;
    }

    /**
     * @param venta the venta to set
     */
    public void setVenta(Venta venta) {
        this.ventas = venta;
    }

    public SucursalControl() {
        sucursales = new ListaEnlazada<>();
        if(listaVentas.isEmpty()){
            this.listaVentas = new ListaEnlazada<>();
            for(EnumMes mes : EnumMes.values()){
                this.ventas = new Venta();
                getVenta().setMes(mes);
                getVenta().setValor(0.0);
                getVenta().setId(id);
                id++;
                listaVentas.insertar(ventas);
            }
        }
    }
    
    

    /**
     * @return the sucursal
     */
    public Sucursal getSucursal() {
        if (sucursal == null) // se crea por si necesito memoria
        {
            sucursal = new Sucursal();
        }
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the sucursales
     */
    public ListaEnlazada<Sucursal> getSucursales() {
        return sucursales;
    }

    /**
     * @param sucursales the sucursales to set
     */
    public void setSucursales(ListaEnlazada<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    public ListaEnlazada<Venta> getVentas() {
        return venta;
    }
    

}
