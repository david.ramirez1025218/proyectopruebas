/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.System.Logger.Level;
import org.jboss.logging.Logger;

/**
 *
 * @author linuxgear
 */
public class AdaptadorDAO<T> implements InterfazDAO<T>  {
    private Conexion conexion;
    private Class clazz;
    public String url;

    public AdaptadorDAO(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";
    }

    public void guardar(T obj) throws IOException {

        ListaEnlazada<T> lista = listar();
        lista.insertar(obj);

        conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
        conexion.getXstream().toXML(lista, new FileWriter(url));
    }

    @Override
    public void modificar(T obj, Integer pos) {
        ListaEnlazada<T> lista = listar();
        
        try{
            lista.modificar(obj, pos);
            
            conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
            conexion.getXstream().toXML(lista, new FileWriter(url));   
            
        }catch(IOException ex){
            System.err.println(ex.getMessage());
        }
    }

    public ListaEnlazada<T> listar() {
        // TODO: el ing implementa arrays yo con listas   

        ListaEnlazada<T> lista = new ListaEnlazada<>();

        try {

            lista = (ListaEnlazada<T>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.out.println(e);
        }
        return lista;
    }

    @Override
    public T obtener(Integer id) {
        ListaEnlazada<T> lista = listar();
        return (T) lista;
    }

}
