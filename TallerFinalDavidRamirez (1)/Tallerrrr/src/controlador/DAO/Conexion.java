/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package controlador.DAO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

/**
 *
 * @author linuxgear
 */
public class Conexion {
 private XStream xstream;
    static String URL = "data/";

    private void conectar() {
        setXstream(new XStream(new JettisonMappedXmlDriver()));
        getXstream().setMode(XStream.NO_REFERENCES);

    }

    /**
     * @return the xstream
     */
    public XStream getXstream() {
        if (xstream == null) 
            conectar();
        
        return xstream;
    }

    /**
     * @param xstream the xstream to set
     */
    public void setXstream(XStream xstream) {
        this.xstream = xstream;
    }

    /**
     * @return the URL
     */
    public static String getURL() {
        return URL;
    }

    /**
     * @param aURL the URL to set
     */
    public static void setURL(String URL) {
        URL = URL;   //aqui h=era aURL
    }
}
