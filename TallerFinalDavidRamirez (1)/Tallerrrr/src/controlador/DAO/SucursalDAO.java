/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import java.io.IOException;
import modelo.Sucursal;

/**
 *
 * @author linuxgear
 */
public class SucursalDAO extends AdaptadorDAO<Sucursal> {
    private Sucursal sucursal;

    public SucursalDAO() {
        super(Sucursal.class);
    }

    /**
     * @return the sucursall
     */
    public Sucursal getSucursal() {
        if (this.sucursal == null) {
            this.sucursal = new Sucursal();
        }
        return sucursal;
    }

    /**
     * @param sucursall the sucursall to set
     */
    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public void guardar() throws IOException {
        this.guardar(sucursal); ///el modificar es lo mismo y solo este debemos replicar porque tenemos qu eenviar el objeto
    }

    public void modificar(Integer pos) {
        this.modificar(sucursal, pos);
    }

    private Integer generateId() {
        return listar().size() + 1;
    }
    
}
