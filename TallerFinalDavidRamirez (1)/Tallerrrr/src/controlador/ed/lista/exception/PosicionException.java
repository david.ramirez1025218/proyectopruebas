/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package controlador.ed.lista.exception;

/**
 *
 * @author cristian
 */
public class PosicionException extends Exception{

    /**
     * Creates a new instance of <code>Vacio</code> without detail message.
     */
    public PosicionException() {
        super("No existe la posicion en tu lista");
    }

    /**
     * Constructs an instance of <code>Vacio</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public PosicionException(String msg) {
        super(msg);
    }
}
