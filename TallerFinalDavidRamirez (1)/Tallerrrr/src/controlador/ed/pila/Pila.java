/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.pila;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.exception.Pilai.TopExcetion;

/**
 *
 * @author linuxgear
 */
public class Pila<E> {
    private Pilai<E> pilai;

    public Pila(Integer cima) {
        pilai = new Pilai<>(cima);
        
    }
    
    public void push(E obj) throws TopExcetion{
        pilai.push(obj);
    }
    
    public E pop() throws VacioException, PosicionException{
        return pilai.pop();
    }
    
    public Integer getCima(){
        return pilai.getCima();
    }
    
    public void Print() throws VacioException{
        pilai.Imprimir();
    }
    
    
}
