/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.pila;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.exception.Pilai.TopExcetion;

/**
 *
 * @author linuxgear
 */
public class Pilai<E>  extends ListaEnlazada<E>{
    private Integer cima;

    public Pilai(Integer cima) {
        this.cima = cima;
    }
    
    public Boolean isFull(){
        return (size() >= cima);
    }
    
    public void push(E info) throws TopExcetion{
        if (!isFull()){
            insertarInicio(info);
        }else{
            throw new TopExcetion();
        }
    }
    
    public E pop() throws VacioException, PosicionException{
        E dato = null;
        if(isEmpty()){
            throw new VacioException("Pila llena");
        }else{
            return this.delete(0);
        }
    }
    
    

    public Integer getCima() {
        return cima;
    }
}








