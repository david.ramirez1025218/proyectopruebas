/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.cola;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.exception.Pilai.TopExcetion;

/**
 *
 * @author linuxgear
 */
public class Cola<E> {
    private Colai<E> cola;

    public Cola(Integer tope) {
        cola = new Colai(tope);
    }
    
    public void queque(E obj) throws TopExcetion{
        cola.queque(obj);
    }
    
    public E dequeque() throws VacioException, PosicionException{
        return cola.dequeque();
    }
    
    public Integer getTope(){
        return cola.getTope();
    }
    
    public void print() throws VacioException{
        cola.Imprimir();
    }
}
