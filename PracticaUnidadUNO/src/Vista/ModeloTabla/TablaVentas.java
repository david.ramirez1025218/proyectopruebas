/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista.ModeloTabla;

import Controlador.Lista.ListaElazada;
import Modelo.Venta;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author linuxgear
 */
public class TablaVentas extends AbstractTableModel{
    private ListaElazada<Venta> datosVentas = new ListaElazada<>();

    public ListaElazada<Venta> getDatosVentas() {
        return datosVentas;
    }

    public void setDatosVentas(ListaElazada<Venta> datosVentas) {
        this.datosVentas = datosVentas;
    }

    @Override
    public int getRowCount() {
        return datosVentas.length;
    }

    @Override
    public int getColumnCount() {   
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Venta s = datosVentas[i];
        switch(i1){
            case 0: return (s != null) ? s.getMes().toString(): "no definido";
            case 1: return (s != null) ? s.getValor(): 0.0;
            default:return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "Meses";
            case 1: return "Valores";
            default:return null;
        }
    }  
}
