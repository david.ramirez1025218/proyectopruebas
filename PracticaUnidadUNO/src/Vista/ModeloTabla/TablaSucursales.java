/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista.ModeloTabla;

import Controlador.Lista.ListaElazada;
import Controlador.Utilidades.Utilidadess;
import Modelo.Sucursales;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author linuxgear
 */
public class TablaSucursales extends AbstractTableModel{
     private ListaElazada<Sucursales> datosSucursales = new ListaElazada<>();

    public ListaElazada<Sucursales> getDatosSucursales() {
        return datosSucursales;
    }

    public void setDatosSucursales(ListaElazada<Sucursales> datosSucursales) {
        this.datosSucursales = datosSucursales;
    }

    @Override
    public int getRowCount() {
        return datosSucursales.length;
    }

    @Override
    public int getColumnCount() {   
        return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Sucursales suc = datosSucursales[i];
        switch(i1){
            case 0: return (suc != null) ? suc.getNombre(): "no definido";
            case 1: return (suc != null) ? Utilidadess.sumarVentas(suc): 0.0;
            case 2: return (suc != null) ? Utilidadess.mediaVentas(suc): 0.0;
            default:return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "Sucursales";
            case 1: return "Ventas Anuales";
            case 2: return "Ventas Promedio";
            default:return null;
        }
    }
}
