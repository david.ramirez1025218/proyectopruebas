/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author linuxgear
 */
public class Venta {
    private Integer id;
    private double valor;
    private EnumMensual mes;

    public Venta() {
    }

    public Venta(Integer id, double valor, EnumMensual mes) {
        this.id = id;
        this.valor = valor;
        this.mes = mes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public EnumMensual getMes() {
        return mes;
    }

    public void setMes(EnumMensual mes) {
        this.mes = mes;
    }
    
    @Override
    public String toString() {
        return mes.toString()+" "+valor;
    }
    
}
