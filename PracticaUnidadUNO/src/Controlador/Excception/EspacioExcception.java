/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Excception;

/**
 *
 * @author linuxgear
 */
public class EspacioExcception extends Exception {

    public EspacioExcception(String message) {
        super(message);
    }

    public EspacioExcception() {
        super("No hay espacio suficiente");
    }
}
