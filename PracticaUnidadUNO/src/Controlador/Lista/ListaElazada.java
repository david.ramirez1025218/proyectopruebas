/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Lista;

import Controlador.Lista.Exception.PosicionExcception;
import Controlador.Lista.Exception.VacioExcception;

/**
 *
 * @author linuxgear
 */
public class ListaElazada<T> {

    private NodoLista<T> cabecera;
    private Integer size = 0;

    public boolean isEmpy() {
        return cabecera == null;
    }

    public void insertar(T info) {

        NodoLista<T> nuevo = new NodoLista<>(info, null);

        if (isEmpy()) {
            this.cabecera = nuevo;
            this.size++;
        } else {
            NodoLista<T> aux = cabecera;

            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            aux.setSig(nuevo);
            this.size++;
        }

    }

    public void insertarInicio(T info) {

        if (isEmpy()) {
            insertar(info);
        } else {
            NodoLista<T> nuevo = new NodoLista<>(info, null);
            nuevo.setSig(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void insertarPosicion(T info, Integer pos) throws PosicionExcception {
        if (isEmpy()) {
            insertar(info);
        } else if (pos >= 0 && pos < size() && pos == 0) {
            insertarInicio(info);
        } else if (pos >= 0 && pos < size()) {
            NodoLista<T> nuevo = new NodoLista<>(info, null);
            NodoLista<T> aux = cabecera;
            for (int i = 0; i < (pos - 1); i++) {
                aux = aux.getSig();
            }
            NodoLista<T> sig = aux.getSig();
            aux.setSig(nuevo);
            nuevo.setSig(sig);
            size++;

        } else {
            throw new PosicionExcception();
        }
    }

    public T get(Integer pos) throws VacioExcception, PosicionExcception {
        if (isEmpy()) {
            throw new VacioExcception();
        } else {
            T dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                } else {
                    NodoLista<T> aux = cabecera;
                    for (int i = 0; i < (pos - 1); i++) {
                        aux = aux.getSig();
                    }
                    dato = aux.getInfo();
                }
            } else {
                throw new PosicionExcception();
            }
            return dato;
        }
    }

    public T delete(Integer pos) throws VacioExcception, PosicionExcception {
        if (isEmpy()) {
            throw new VacioExcception();
        } else {
            T dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                    cabecera = cabecera.getSig();
                    size--;
                } else {
                    NodoLista<T> aux = cabecera;
                    for (int i = 0; i < (pos - 1); i++) {
                        aux = aux.getSig();
                    }

                    NodoLista<T> aux1 = aux.getSig();
                    dato = aux1.getInfo();

                    NodoLista<T> proximo = aux.getSig();
                    aux.setSig(proximo.getSig());
                    size--;
                }
            } else {
                throw new PosicionExcception();
            }
            return dato;
        }
    }

    public void deleteAll() {
        this.cabecera = null;
    }

    public Integer size() {
        return size;
    }

    public void Imprimir() throws VacioExcception {
        if (isEmpy()) {
            throw new VacioExcception();
        } else {
            NodoLista<T> aux = cabecera;
            System.out.println("-----------");
            System.out.println("---Lista----");
            System.out.println("-----------");
            for (int i = 0; i < size(); i++) {
                System.out.println(aux.getInfo());
                aux = aux.getSig();
            }
            System.out.println("------------------");
            System.out.println("----Lista Fin----");
            System.out.println("------------------");
        }

    }

    public NodoLista getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista cabecera) {
        this.cabecera = cabecera;
    }

}
