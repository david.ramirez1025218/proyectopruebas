/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Lista;

/**
 *
 * @author linuxgear
 */
public class NodoLista <T> {
    private T info;
    private NodoLista sig;
    
    
    

    public NodoLista() {
        info = null;
        sig = null;
    }

    public NodoLista(T info, NodoLista sig){
        this.info = info;
        this.sig = sig;
    }
    
    
    
    
    
    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public NodoLista getSig() {
        return sig;
    }

    public void setSig(NodoLista sig) {
        this.sig = sig;
    }
    
}
