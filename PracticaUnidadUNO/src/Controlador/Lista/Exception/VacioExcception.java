/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Lista.Exception;

/**
 *
 * @author linuxgear
 */
public class VacioExcception extends Exception{

    public VacioExcception() {
        super("Lista Vacia");
    }
    public VacioExcception(String msg) {
        super(msg);
    }
}
