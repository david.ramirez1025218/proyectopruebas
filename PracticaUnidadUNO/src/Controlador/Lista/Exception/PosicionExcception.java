/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Lista.Exception;

/**
 *
 * @author linuxgear
 */
public class PosicionExcception extends Exception{
    
    public PosicionExcception() {
        super("No existe la posicion en tu lista");
    }

    public PosicionExcception(String msg) {
        super(msg);
    }
}
