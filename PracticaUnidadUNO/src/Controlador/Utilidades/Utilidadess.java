/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Utilidades;

import Modelo.Sucursales;
import Modelo.Venta;

/**
 *
 * @author linuxgear
 */
public class Utilidadess {

    public static void ImprimirUtilidades(Object[] obj) {
        System.out.println("Mostrando lista de: " + obj.getClass().getSimpleName());
        for (Object ob : obj) {
            System.out.println(ob.toString());
        }
    }

    public static Double sumarVentas(Sucursales s) {
        if (s.getVentas() != null) {
            Double ventas = 0.0;
            for (Venta v : s.getVentas()) {
                ventas += v.getValor();
            }
            return ventas;
        }
        return 0.0;
    }

    public static Double mediaVentas(Sucursales s) {
        Double suma = sumarVentas(s);
        if (suma == 0) {
            return suma;
        } else {
            return suma / (s.getVentas().length);
        }
    }
}
