/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Controlador.Excception.EspacioExcception;
import Modelo.EnumMensual;
import Modelo.Sucursales;
import Modelo.Venta;

/**
 *
 * @author linuxgear
 */
public class SucursalesControl {
    private Sucursales[] sucursales;
    private Sucursales sucursal;
    private Venta ventas;
    
    public boolean registrar() throws EspacioExcception {
        int pos = -1;
        int cont = -1;

        for (Sucursales s : getSucursales()) {
            cont++;
            if (s == null) {
                pos = cont;
                break;
            }

        }

        if (pos == -1) {
            throw new EspacioExcception();
        }

        
        sucursal.setVentas(new Venta[EnumMensual.values().length]);
        for (int i = 0; i < EnumMensual.values().length; i++) {
            Venta venta = new Venta();
            venta.setId(i + 1);
            venta.setMes(EnumMensual.values()[i]);
            venta.setValor(0.0);
            sucursal.getVentas()= venta;
        }
        
        sucursales[pos] = sucursal;
        return true;
    }
    
    public boolean guardarVentas(Integer posVenta, Double valor) throws EspacioExcception {
        
        if(this.sucursal != null){
            if(posVenta <= this.sucursal.getVentas().length-1)
                sucursal.getVentas()[posVenta].setValor(valor);
            else
                throw new EspacioExcception();
        }else
            throw new NullPointerException("Debe seleccionar una sucursal");
        
        return true;
    }
    
    
    
    
    
    

    public SucursalesControl() {
        sucursales = new Sucursales[4];
    }

    public SucursalesControl(Sucursales[] sucursales, Sucursales sucursal, Venta ventas) {
        this.sucursales = sucursales;
        this.sucursal = sucursal;
        this.ventas = ventas;
    }

    public Sucursales[] getSucursales() {
        return sucursales;
    }

    public void setSucursales(Sucursales[] sucursales) {
        this.setSucursales(sucursales);
    }

    public Sucursales getSucursal() {
        if (sucursal == null) 
        {
            sucursal = new Sucursales();
        }
        return sucursal;
    }

    public void setSucursal(Sucursales sucursal) {
        this.sucursal = sucursal;
    }

    public Venta getVentas() {
        return ventas;
    }

    public void setVentas(Venta ventas) {
        this.ventas = ventas;
    }
    
    
}
