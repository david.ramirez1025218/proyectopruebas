/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.util;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author crsitian
 */
public class Utilidades {
    public static Double sumarVentas(Sucursal s) {
        double suma = 0.0;
        ListaEnlazada<Venta> listaVentas = s.getVentas();

        NodoLista<Venta> nodo = listaVentas.getCabecera();
        while (nodo != null) {
            Venta venta = nodo.getInfo();
            suma += venta.getValor();
            nodo = nodo.getSig();
        }

        return suma;
    }
    
    public static Double mediaVentas(Sucursal s){
        Double suma = sumarVentas(s);
        ListaEnlazada<Venta> listaVentas = s.getVentas();
        
        if (suma == 0) {
            return suma;
        } else {
            return suma / listaVentas.size();
        }
    }
}
