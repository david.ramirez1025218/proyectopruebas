/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Exception.java to edit this template
 */
package controlador.ed.lista.exception;

/**
 *
 * @author cristian
 */
public class TopeException extends Exception{

    /**
     * Creates a new instance of <code>Vacio</code> without detail message.
     */
    public TopeException() {
        super("No existe más espacio en la cola");
    }

    /**
     * Constructs an instance of <code>Vacio</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public TopeException(String msg) {
        super(msg);
    }
}
