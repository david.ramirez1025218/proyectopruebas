/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author 
 */
public class Cola<E> {
    private ColaImplementacion<E> cola;
    
    public Cola(Integer tope) {
        cola = new ColaImplementacion<>(tope);
    }
    
    public void queue(E obj) throws TopeException{
        cola.queue(obj);
    }
    
    public E dequeue() throws VacioException, PosicionException{
        return  cola.dequeue();
    }
    
    public Integer getTope(){
        return cola.getTope();
    }
    
    public void print () throws VacioException{
        cola.Imprimir();
    }

    /**
     * @return the cola
     */
    public ColaImplementacion<E> getCola() {
        return cola;
    }

    /**
     * @param cola the cola to set
     */
    public void setCola(ColaImplementacion<E> cola) {
        this.cola = cola;
    }
    
    
}
