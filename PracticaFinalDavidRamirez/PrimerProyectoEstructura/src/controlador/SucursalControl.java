/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.ed.lista.ListaEnlazada;
import controlador.exception.EsapacioException;
import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author crsitian
 */
public class SucursalControl {

    private ListaEnlazada<Sucursal> sucursales;
    private Sucursal sucursal;
    private Venta venta;
    private ListaEnlazada<Venta> ventas;

    public SucursalControl() {
        sucursales = new ListaEnlazada<>();
        ventas = new ListaEnlazada<>();
    }
    
    public SucursalControl(ListaEnlazada<Sucursal> sucursales, Sucursal sucursal, Venta venta, ListaEnlazada<Venta> ventas) {
        this.sucursales = sucursales;
        this.sucursal = sucursal;
        this.venta = venta;
        this.ventas = ventas;
    }
    
    
    
    
    
    
    
    /*
    public boolean registrar() throws EsapacioException {
        int pos = -1;
        int cont = -1;

        for (Sucursal s : getSucursales()) {
            cont++;
            if (s == null) {
                pos = cont;
                break;
            }

        }

        if (pos == -1) {
            throw new EsapacioException();
        }

        
        sucursal.setVentas(new Venta[EnumMes.values().length]);
        for (int i = 0; i < EnumMes.values().length; i++) {
            Venta venta = new Venta();
            venta.setId(i + 1);
            venta.setMes(EnumMes.values()[i]);
            venta.setValor(0.0);
            sucursal.getVentas()[i] = venta;
        }
        
        sucursales[pos] = sucursal;
        return true;
    }
    
    public boolean guardarVentas(Integer posVenta, Double valor) throws EsapacioException {
        
        if(this.sucursal != null){
            if(posVenta <= this.sucursal.getVentas().length-1)
                sucursal.getVentas()[posVenta].setValor(valor);
            else
                throw new EsapacioException();
        }else
            throw new NullPointerException("Debe seleccionar una sucursal");
        
        return true;
    }
    

  
    */

    /**
     * @return the sucursales
     */
    public ListaEnlazada<Sucursal> getSucursales() {
        return sucursales;
    }

    /**
     * @param sucursales the sucursales to set
     */
    public void setSucursales(ListaEnlazada<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    /**
     * @return the sucursal
     */
    public Sucursal getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the venta
     */
    public Venta getVenta() {
        return venta;
    }

    /**
     * @param venta the venta to set
     */
    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    /**
     * @return the ventas
     */
    public ListaEnlazada<Venta> getVentas() {
        return ventas;
    }

    /**
     * @param ventas the ventas to set
     */
    public void setVentas(ListaEnlazada<Venta> ventas) {
        this.ventas = ventas;
    }

    

}
