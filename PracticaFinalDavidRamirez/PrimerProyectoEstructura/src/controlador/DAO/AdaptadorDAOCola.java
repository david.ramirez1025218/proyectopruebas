/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ed.cola.Cola;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.TopeException;
import controlador.ed.lista.exception.VacioException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author
 */
public class AdaptadorDAOCola<T> {
    private Conexion conexion;
    private Class clazz;
    private String url;

    public AdaptadorDAOCola(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";

    }

    public void guardar(T obj) throws TopeException {
        try {
            Cola<T> cola = listar();
            cola.queue(obj);
            conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
            conexion.getXstream().toXML(cola, new FileWriter(url));
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void modificar(T obj, Integer pos) {
        Cola<T> cola = listar();
        try {
            cola.getCola().modificar(obj, pos);

            conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
            conexion.getXstream().toXML(cola, new FileWriter(url));
        } catch (PosicionException | IOException ex) {
            System.err.println(ex.getMessage());
        }

    }
    
    public void borrar() throws VacioException, PosicionException{
        try {
            Cola<T> cola = listar();
            cola.dequeue();
            conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
            conexion.getXstream().toXML(cola, new FileWriter(url));
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public T obtener(Integer id) {
        Cola<T> cola = listar();
        return (T) cola;
    }

    public Cola<T> listar() {
        Cola<T> cola = new Cola<>(10);
        try {
            cola = (Cola<T>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return cola;
    }
}
