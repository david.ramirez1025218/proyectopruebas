/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modelo.tabla;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.util.Utilidades;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import modelo.Venta;

/**
 *
 * @author crsitian
 */
public class ModeloTablaVentas extends AbstractTableModel{
    
    private ListaEnlazada<Venta> datos = new ListaEnlazada<>();


    @Override
    public int getRowCount() {
        return getDatos().size();
    }

    @Override
    public int getColumnCount() {   
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        try {
            Venta s = getDatos().get(i);
            switch(i1){
                case 0: return (s != null) ? s.getMes().toString(): "no definido";
                case 1: return (s != null) ? s.getValor(): 0.0;
                default:return null;
            }
        } catch (VacioException ex) {
            System.err.println(ex.getMessage());
        } catch (PosicionException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "Mes";
            case 1: return "Valor";
            default:return null;
        }
    }

    /**
     * @return the datos
     */
    public ListaEnlazada<Venta> getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(ListaEnlazada<Venta> datos) {
        this.datos = datos;
    }

    
}
