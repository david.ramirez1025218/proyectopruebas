/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modelo.tabla;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.util.Utilidades;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.AbstractTableModel;
import modelo.Sucursal;

/**
 *
 * @author crsitian
 */
public class ModeloTablaSucursal extends AbstractTableModel {

    private ListaEnlazada<Sucursal> datos = new ListaEnlazada<>();

    @Override
    public int getRowCount() {
        return getDatos().size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Sucursal s;
        try {
            s = getDatos().get(i);
            switch (i1) {
                case 0:
                    return (s != null) ? s.getNombre() : "no definido";
                case 1:
                    return (s != null) ? Utilidades.sumarVentas(s) : 0.0;
                case 2:
                    return (s != null) ? Utilidades.mediaVentas(s) : 0.0;
                default:
                    return null;
            }
        } catch (VacioException ex) {
            Logger.getLogger(ModeloTablaSucursal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(ModeloTablaSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Sucursal";
            case 1:
                return "Venta Anual";
            case 2:
                return "Venta Promedio";
            default:
                return null;
        }
    }

    /**
     * @return the datos
     */
    public ListaEnlazada<Sucursal> getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(ListaEnlazada<Sucursal> datos) {
        this.datos = datos;
    }

}
