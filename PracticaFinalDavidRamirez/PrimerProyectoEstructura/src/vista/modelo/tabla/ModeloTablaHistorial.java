/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modelo.tabla;

import controlador.ed.cola.Cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import javax.swing.table.AbstractTableModel;
import modelo.Historial;

/**
 *
 * @author 
 */
public class ModeloTablaHistorial extends AbstractTableModel{
    private Cola<Historial> dato = new Cola<>(10); 

    public ModeloTablaHistorial() {
    }
    
    
    @Override
    public int getRowCount() {
        return dato.getCola().size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        try {
            Historial h = dato.getCola().get(i);
            switch(i1){
                case 0: return (h != null) ? h.getFecha().getDate(): "no definido";
                case 1: return (h != null) ? h.getVenta().getMes().name(): "no definido";
                case 2: return (h != null) ? h.getVenta().getValor(): "no definido";
                default:return null;
            }
        } catch (VacioException ex) {
            System.err.println(ex.getMessage());
        } catch (PosicionException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0: return "Día en que se hizo el cambio";
            case 1: return "Mes de la venta";
            case 2: return "Valor cambiado";
            default:return null;
        }
    }

    /**
     * @return the dato
     */
    public Cola<Historial> getDato() {
        return dato;
    }

    /**
     * @param dato the dato to set
     */
    public void setDato(Cola<Historial> dato) {
        this.dato = dato;
    }
    
}
